#!/bin/bash

sudo apt-get update
sudo apt-get install nginx

sudo bash -c "cat > /etc/nginx/sites-available/gold_app.conf <<EOF
server {
    listen 80;
    
    server_name gold.kampus-inggris.com;

    location / {
        proxy_pass http://localhost:8081;
        proxy_set_header Host \$host;
        proxy_set_header X-Real-IP \$remote_addr;
    }

    location /users {
        proxy_pass http://localhost:4000;
        proxy_set_header Host \$host;
        proxy_set_header X-Real-IP \$remote_addr;
    }
}
EOF"

# Create a symbolic link to the virtual host configuration in the sites-enabled directory
sudo ln -s /etc/nginx/sites-available/gold_app.conf /etc/nginx/sites-enabled/

# Install Certbot
sudo apt-get install certbot python3-certbot-nginx

# Obtain an SSL certificate for the domain
sudo certbot --nginx -d gold.kampus-inggris.com

# Restart Nginx to activate the virtual host configuration
sudo service nginx restart
